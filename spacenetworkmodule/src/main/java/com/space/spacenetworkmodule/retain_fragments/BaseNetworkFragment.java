package com.space.spacenetworkmodule.retain_fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.space.spacenetworkmodule.R;
import com.space.spacenetworkmodule.common.Utils;
import com.space.spacenetworkmodule.data.network.BaseNetworkTaskCallbacks;
import com.space.spacenetworkmodule.data.network.NetworkRequestID;
import com.space.spacenetworkmodule.data.network.SpcNetworkRequest;
import com.space.spacenetworkmodule.data.network.SpcNetworkRequest.Status;
import com.space.spacenetworkmodule.data.network.res.BaseResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

@SuppressWarnings("unused")
public class BaseNetworkFragment extends Fragment {

    public final String TAG = "DEV" + getClass().getSimpleName();

    public static volatile List<SpcNetworkRequest> mNetworkRequests = new ArrayList<>();

    public BaseNetworkTaskCallbacks mCallbacks;

    //region :::::::::::::::::::::::::::::::::::::::::: Utils
    public boolean isNetworkAvailable() {
        if (!Utils.isNetworkAvailable(getContext())) {
            if (mCallbacks != null)
                mCallbacks.onNetworkRequestFailed(
                        new SpcNetworkRequest().failed(getString(R.string.error_no_network_connection), true, false));
            return false;
        }
        return true;
    }

    public boolean isExecutePossible(NetworkRequestID reqId, Object additionalInfo) {
        SpcNetworkRequest req = findRequest(reqId, additionalInfo);
        return req == null || req.getStatus() != Status.RUNNING;
    }

    public boolean isExecutePossible(NetworkRequestID reqId) {
        SpcNetworkRequest req = findRequest(reqId);
        return req == null || req.getStatus() != Status.RUNNING;
    }

    public void addRequest(SpcNetworkRequest req) {
        mNetworkRequests.add(req);
    }

    @Nullable
    public SpcNetworkRequest findRequest(NetworkRequestID reqId) {
        for (SpcNetworkRequest n : mNetworkRequests) {
            if (n.getId() == reqId)
                return n;
        }
        return null;
    }

    @Nullable
    public SpcNetworkRequest findRequest(NetworkRequestID reqId, Object additionalInfo) {
        for (SpcNetworkRequest n : mNetworkRequests) {
            if (n.getId() == reqId && Utils.equals(n.getAdditionalInfo(), additionalInfo))
                return n;
        }
        return null;
    }

    public void removeRequest(@Nullable SpcNetworkRequest request) {
        if (request == null) return;
        SpcNetworkRequest req = findRequest(request.getId(), request.getAdditionalInfo());
        if (req != null) mNetworkRequests.remove(req);
    }
    //endregion ::::::::::::::::::::::::::::::::::::::::::

    //region :::::::::::::::::::::::::::::::::::::::::: Fragment Life Cycle
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        for (SpcNetworkRequest n : mNetworkRequests) {
            if (mCallbacks != null && n.getStatus() == Status.FINISHED) {
                if (!n.isCancelled()) {
                    mCallbacks.onNetworkRequestFinished(n);
                } else {
                    mCallbacks.onNetworkRequestFailed(n);
                }
                removeRequest(n);
            }
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof BaseNetworkTaskCallbacks) {
            mCallbacks = (BaseNetworkTaskCallbacks) activity;
        } else {
            throw new IllegalStateException("Parent activity must implement BaseNetworkTaskCallbacks");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    //endregion ::::::::::::::::::::::::::::::::::::::::::

    //region :::::::::::::::::::::::::::::::::::::::::: Request status
    public void onRequestStarted(@NonNull SpcNetworkRequest request) {
        addRequest(request);
        request.started();
        if (mCallbacks != null) mCallbacks.onNetworkRequestStarted(request);
    }

    public void onRequestResponseEmpty(@NonNull SpcNetworkRequest request) {
        request.failed(getString(R.string.error_response_is_empty), false, false);
        if (mCallbacks != null) {
            mCallbacks.onNetworkRequestFailed(request);
            removeRequest(request);
        }
    }

    public void onRequestComplete(@NonNull SpcNetworkRequest request) {
        request.successful();
        if (mCallbacks != null) {
            mCallbacks.onNetworkRequestFinished(request);
            removeRequest(request);
        }
    }

    public <T extends BaseResponse> void onRequestHttpError(@NonNull SpcNetworkRequest request, Response<T> error) {
        request.failed(String.valueOf(error.errorBody().source()), true, false);
        if (mCallbacks != null) {
            mCallbacks.onNetworkRequestFailed(request);
            removeRequest(request);
        }
    }

    public void onRequestFailure(@NonNull SpcNetworkRequest request, Throwable t) {
        if (!Utils.isEmptyOrNull(t.getMessage())) {
            request.failed(String.format("%s: %s", getString(R.string.error_unknown_response), t.getMessage()), true, false);
        } else {
            request.failed(getString(R.string.error_connection_failed), true, false);
        }
        if (mCallbacks != null) {
            mCallbacks.onNetworkRequestFailed(request);
            removeRequest(request);
        }
    }

//endregion ::::::::::::::::::::::::::::::::::::::::::

    public void printRequestTable() {
        for (SpcNetworkRequest n : mNetworkRequests) {
            Log.d(TAG, "==================================== " + n.getId());
            Log.d(TAG, n.toString());
        }
    }
}
