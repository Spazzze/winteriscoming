package com.space.spacenetworkmodule.data.network;

import android.support.annotation.NonNull;

public interface BaseNetworkTaskCallbacks {

    void onNetworkRequestStarted(@NonNull SpcNetworkRequest request);

    void onNetworkRequestFinished(@NonNull SpcNetworkRequest request);

    void onNetworkRequestFailed(@NonNull SpcNetworkRequest request);
}
