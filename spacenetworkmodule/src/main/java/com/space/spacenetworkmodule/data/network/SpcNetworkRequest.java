package com.space.spacenetworkmodule.data.network;

import retrofit2.Response;

@SuppressWarnings("unused")
public class SpcNetworkRequest {

    public enum Status {
        PENDING,
        RUNNING,
        FINISHED,
    }

    private Status mStatus = Status.PENDING;
    private NetworkRequestID mID = NetworkRequestID.DEFAULT;
    private String mError = null;
    private boolean mCancelled = false;
    private boolean isAnnounceError = false;
    private boolean isErrorCritical = false;
    private Object mAdditionalInfo = null;
    private Response mResponse = null;

    public SpcNetworkRequest() {
    }

    public SpcNetworkRequest(NetworkRequestID NetworkRequestID) {
        this.mID = NetworkRequestID;
    }

    public SpcNetworkRequest(NetworkRequestID NetworkRequestID, Object additionalInfo) {
        mID = NetworkRequestID;
        mAdditionalInfo = additionalInfo;
    }

    public SpcNetworkRequest started() {
        this.mStatus = Status.RUNNING;
        this.mError = null;
        this.mCancelled = false;
        this.isAnnounceError = false;
        this.isErrorCritical = false;
        return this;
    }

    public SpcNetworkRequest failed(String error, boolean isAnnounceError, boolean isErrorCritical) {
        this.mStatus = Status.FINISHED;
        this.mError = error;
        this.mCancelled = true;
        this.isAnnounceError = isAnnounceError;
        this.isErrorCritical = isErrorCritical;
        return this;
    }

    public SpcNetworkRequest critical(String error) {
        this.mStatus = Status.FINISHED;
        this.mError = error;
        this.mCancelled = true;
        this.isAnnounceError = true;
        this.isErrorCritical = true;
        return this;
    }

    public SpcNetworkRequest successful() {
        this.mStatus = Status.FINISHED;
        this.mError = null;
        this.mCancelled = false;
        this.isAnnounceError = false;
        this.isErrorCritical = false;
        return this;
    }

    //region :::::::::::::::::::::::::::::::::::::::::: Getters

    public Object getAdditionalInfo() {
        return mAdditionalInfo;
    }

    public NetworkRequestID getId() {
        return mID;
    }

    public Status getStatus() {
        return mStatus;
    }

    public String getError() {
        return mError;
    }

    public Response getResponse() {
        return mResponse;
    }

    public boolean isCancelled() {
        return mCancelled;
    }

    public boolean isAnnounceError() {
        return isAnnounceError;
    }

    public boolean isErrorCritical() {
        return isErrorCritical;
    }
    //endregion :::::::::::::::::::::::::::::::::::::::::: Getters

    //region :::::::::::::::::::::::::::::::::::::::::: Setters

    public void setResponse(Response response) {
        mResponse = response;
    }

    public void setAdditionalInfo(Object additionalInfo) {
        this.mAdditionalInfo = additionalInfo;
    }

    public void setNetworkRequestIDs(NetworkRequestID NetworkRequestID) {
        this.mID = NetworkRequestID;
    }

    public void setStatus(Status status) {
        mStatus = status;
    }

    public void setError(String error) {
        mError = error;
    }

    public void setCancelled(boolean cancelled) {
        mCancelled = cancelled;
    }

    public void setAnnounceError(boolean announceError) {
        isAnnounceError = announceError;
    }

    public void setErrorCritical(boolean errorCritical) {
        isErrorCritical = errorCritical;
    }
    //endregion :::::::::::::::::::::::::::::::::::::::::: Setters

    @Override
    public String toString() {
        return "SpcNetworkRequest{" +
                "mStatus=" + mStatus +
                ", mID=" + mID +
                ", mError='" + mError + '\'' +
                ", mCancelled=" + mCancelled +
                ", isAnnounceError=" + isAnnounceError +
                ", isErrorCritical=" + isErrorCritical +
                ", mAdditionalInfo class =" + (mAdditionalInfo != null ? mAdditionalInfo.getClass() : "") +
                ", mAdditionalInfo=" + mAdditionalInfo +
                '}';
    }
}
