package com.space.spacenetworkmodule.data.network;

public enum NetworkRequestID {

    DEFAULT,
    CHARACTER,
    CHARACTERS,
    ALL_CHARACTERS,
    BOOK,
    BOOKS,
    HOUSE,
    HOUSES,

}
