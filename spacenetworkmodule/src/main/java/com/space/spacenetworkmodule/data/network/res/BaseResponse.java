package com.space.spacenetworkmodule.data.network.res;

import java.util.List;

public class BaseResponse<T> {
    List<T> data;

    public List<T> getData() {
        return data;
    }
}

