package com.space.winteriscoming.data.storage.models;

import com.space.winteriscoming.data.network.dto.House;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.JoinProperty;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.ToMany;
import org.greenrobot.greendao.annotation.Unique;

import java.util.List;

import static com.space.winteriscoming.utils.AppUtils.getCharacterId;
import static com.space.winteriscoming.utils.AppUtils.getHouseId;

@Entity(active = true, nameInDb = "HOUSES")
public class HouseEntity {

    @Id
    private Long id;

    @NotNull
    @Unique
    private String remoteId;

    @NotNull
    @Unique
    private String name;

    private String words;
    private String currentLord;
    private String founder;
    private String founded;
    private String diedOut;

    @ToMany(joinProperties = {
            @JoinProperty(name = "remoteId", referencedName = "mainHouseId")
    })
    private List<CharacterEntity> swornMembers;

    public HouseEntity(House h) {

        this.remoteId = getHouseId(h.getUrl());
        this.name = h.getName();
        this.words = h.getWords();

        if (h.getCurrentLord() != null)
            this.currentLord = getCharacterId(h.getCurrentLord());
        if (h.getFounder() != null)
            this.founder = getCharacterId(h.getFounder());

        this.founded = h.getFounded();
        this.diedOut = h.getDiedOut();
    }

    //region Generated
    /**
     * Used to resolve relations
     */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /**
     * Used for active entity operations.
     */
    @Generated(hash = 20667771)
    private transient HouseEntityDao myDao;

    @Generated(hash = 188584759)
    public HouseEntity(Long id, @NotNull String remoteId, @NotNull String name,
                       String words, String currentLord, String founder, String founded,
                       String diedOut) {
        this.id = id;
        this.remoteId = remoteId;
        this.name = name;
        this.words = words;
        this.currentLord = currentLord;
        this.founder = founder;
        this.founded = founded;
        this.diedOut = diedOut;
    }

    @Generated(hash = 375157870)
    public HouseEntity() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRemoteId() {
        return this.remoteId;
    }

    public void setRemoteId(String remoteId) {
        this.remoteId = remoteId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWords() {
        return this.words;
    }

    public void setWords(String words) {
        this.words = words;
    }

    public String getCurrentLord() {
        return this.currentLord;
    }

    public void setCurrentLord(String currentLord) {
        this.currentLord = currentLord;
    }

    public String getFounder() {
        return this.founder;
    }

    public void setFounder(String founder) {
        this.founder = founder;
    }

    public String getFounded() {
        return this.founded;
    }

    public void setFounded(String founded) {
        this.founded = founded;
    }

    public String getDiedOut() {
        return this.diedOut;
    }

    public void setDiedOut(String diedOut) {
        this.diedOut = diedOut;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 1813775308)
    public List<CharacterEntity> getSwornMembers() {
        if (swornMembers == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            CharacterEntityDao targetDao = daoSession.getCharacterEntityDao();
            List<CharacterEntity> swornMembersNew = targetDao
                    ._queryHouseEntity_SwornMembers(remoteId);
            synchronized (this) {
                if (swornMembers == null) {
                    swornMembers = swornMembersNew;
                }
            }
        }
        return swornMembers;
    }

    /**
     * Resets a to-many relationship, making the next get call to query for a fresh result.
     */
    @Generated(hash = 264415194)
    public synchronized void resetSwornMembers() {
        swornMembers = null;
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    
/** called by internal mechanisms, do not call yourself. */
@Generated(hash = 1246536709)
public void __setDaoSession(DaoSession daoSession) {
    this.daoSession = daoSession;
    myDao = daoSession != null ? daoSession.getHouseEntityDao() : null;
}
}
