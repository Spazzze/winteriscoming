package com.space.winteriscoming.data.network.dto;

import com.google.gson.annotations.SerializedName;
import com.space.spacenetworkmodule.data.network.res.BaseResponse;

import java.util.ArrayList;
import java.util.List;

public class House extends BaseResponse {
    @SerializedName("url")
    private String url;
    @SerializedName("name")
    private String name;
    @SerializedName("region")
    private String region;
    @SerializedName("coatOfArms")
    private String coatOfArms;
    @SerializedName("words")
    private String words;
    @SerializedName("titles")
    private List<String> titles = new ArrayList<>();
    @SerializedName("seats")
    private List<String> seats = new ArrayList<>();
    @SerializedName("currentLord")
    private String currentLord;
    @SerializedName("heir")
    private String heir;
    @SerializedName("overlord")
    private String overlord;
    @SerializedName("founded")
    private String founded;
    @SerializedName("founder")
    private String founder;
    @SerializedName("diedOut")
    private String diedOut;
    @SerializedName("ancestralWeapons")
    private List<String> ancestralWeapons = new ArrayList<>();
    @SerializedName("cadetBranches")
    private List<String> cadetBranches = new ArrayList<>();
    @SerializedName("swornMembers")
    private List<String> swornMembers = new ArrayList<>();

    //region Getters
    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public String getRegion() {
        return region;
    }

    public String getCoatOfArms() {
        return coatOfArms;
    }

    public String getWords() {
        return words;
    }

    public List<String> getTitles() {
        return titles;
    }

    public List<String> getSeats() {
        return seats;
    }

    public String getCurrentLord() {
        return currentLord;
    }

    public String getHeir() {
        return heir;
    }

    public String getOverlord() {
        return overlord;
    }

    public String getFounded() {
        return founded;
    }

    public String getFounder() {
        return founder;
    }

    public String getDiedOut() {
        return diedOut;
    }

    public List<String> getAncestralWeapons() {
        return ancestralWeapons;
    }

    public List<String> getCadetBranches() {
        return cadetBranches;
    }

    public List<String> getSwornMembers() {
        return swornMembers;
    }
    //endregion

    //region Setters
    public void setUrl(String url) {
        this.url = url;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setCoatOfArms(String coatOfArms) {
        this.coatOfArms = coatOfArms;
    }

    public void setWords(String words) {
        this.words = words;
    }

    public void setTitles(List<String> titles) {
        this.titles = titles;
    }

    public void setSeats(List<String> seats) {
        this.seats = seats;
    }

    public void setCurrentLord(String currentLord) {
        this.currentLord = currentLord;
    }

    public void setHeir(String heir) {
        this.heir = heir;
    }

    public void setOverlord(String overlord) {
        this.overlord = overlord;
    }

    public void setFounded(String founded) {
        this.founded = founded;
    }

    public void setFounder(String founder) {
        this.founder = founder;
    }

    public void setDiedOut(String diedOut) {
        this.diedOut = diedOut;
    }

    public void setAncestralWeapons(List<String> ancestralWeapons) {
        this.ancestralWeapons = ancestralWeapons;
    }

    public void setCadetBranches(List<String> cadetBranches) {
        this.cadetBranches = cadetBranches;
    }

    public void setSwornMembers(List<String> swornMembers) {
        this.swornMembers = swornMembers;
    }
    //endregion
}