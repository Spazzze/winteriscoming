package com.space.winteriscoming.data.storage.models;

import android.text.TextUtils;

import com.space.winteriscoming.data.network.dto.Person;
import com.space.winteriscoming.utils.AppUtils;
import com.space.winteriscoming.utils.Const;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Unique;

import static com.space.winteriscoming.utils.AppUtils.getCharacterId;

@Entity(active = true, nameInDb = "CHARACTERS")
public class CharacterEntity {

    @Id
    private Long id;

    @NotNull
    @Unique
    private String remoteId;

    @NotNull
    @Unique
    private String name = "";

    private String words = "";
    private String born = "";
    private String died = "";
    private String titles = "";
    private String aliases = "";
    private String father = "";
    private String mother = "";
    private String spouse = "";

    @NotNull
    private String mainHouseId = "";

    public CharacterEntity(Person c, String mainHouseId, String words) {
        if (c == null) return;
        this.mainHouseId = mainHouseId;
        this.words = words;
        this.remoteId = getCharacterId(c.getUrl());
        this.name = c.getName();
        this.born = c.getBorn();
        this.died = c.getDied();
        this.titles = TextUtils.join("\n", c.getTitles());
        this.aliases = TextUtils.join("\n", c.getAliases());
        if (c.getFather() != null)
            this.father = getCharacterId(c.getFather());
        if (c.getMother() != null)
            this.mother = getCharacterId(c.getMother());
        if (c.getSpouse() != null)
            this.spouse = getCharacterId(c.getSpouse());
    }

    public CharacterEntity(Person c) {
        if (c == null) return;
        this.remoteId = getCharacterId(c.getUrl());
        this.name = c.getName();
        this.born = c.getBorn();
        this.died = c.getDied();
        this.titles = TextUtils.join("\n", c.getTitles());
        this.aliases = TextUtils.join("\n", c.getAliases());
        if (c.getFather() != null)
            this.father = getCharacterId(c.getFather());
        if (c.getMother() != null)
            this.mother = getCharacterId(c.getMother());
        if (c.getSpouse() != null)
            this.spouse = getCharacterId(c.getSpouse());
        for (String s : c.getAllegiances()) {
            String houseId = AppUtils.getHouseId(s);
            if (Const.HOUSES_IDS.contains(houseId)) {
                mainHouseId = houseId;
                break;
            }
        }
    }

    //region Generated
    /**
     * Used to resolve relations
     */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /**
     * Used for active entity operations.
     */
    @Generated(hash = 1738962208)
    private transient CharacterEntityDao myDao;

    @Generated(hash = 2005348552)
    public CharacterEntity(Long id, @NotNull String remoteId, @NotNull String name, String words,
                           String born, String died, String titles, String aliases, String father, String mother,
                           String spouse, @NotNull String mainHouseId) {
        this.id = id;
        this.remoteId = remoteId;
        this.name = name;
        this.words = words;
        this.born = born;
        this.died = died;
        this.titles = titles;
        this.aliases = aliases;
        this.father = father;
        this.mother = mother;
        this.spouse = spouse;
        this.mainHouseId = mainHouseId;
    }

    @Generated(hash = 788197839)
    public CharacterEntity() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRemoteId() {
        return this.remoteId;
    }

    public void setRemoteId(String remoteId) {
        this.remoteId = remoteId;
    }

    public String getBorn() {
        return this.born;
    }

    public void setBorn(String born) {
        this.born = born;
    }

    public String getDied() {
        return this.died;
    }

    public void setDied(String died) {
        this.died = died;
    }

    public String getTitles() {
        return this.titles;
    }

    public void setTitles(String titles) {
        this.titles = titles;
    }

    public String getAliases() {
        return this.aliases;
    }

    public void setAliases(String aliases) {
        this.aliases = aliases;
    }

    public String getFather() {
        return this.father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    public String getMother() {
        return this.mother;
    }

    public void setMother(String mother) {
        this.mother = mother;
    }

    public String getSpouse() {
        return this.spouse;
    }

    public void setSpouse(String spouse) {
        this.spouse = spouse;
    }

    public String getMainHouseId() {
        return this.mainHouseId;
    }

    public void setMainHouseId(String mainHouseId) {
        this.mainHouseId = mainHouseId;
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWords() {
        return this.words;
    }

    public void setWords(String words) {
        this.words = words;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 35488055)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getCharacterEntityDao() : null;
    }

    //endregion
}
