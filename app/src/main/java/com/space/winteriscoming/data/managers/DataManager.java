package com.space.winteriscoming.data.managers;

import android.support.annotation.NonNull;
import android.util.Log;

import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.space.winteriscoming.common.CustomApp;
import com.space.winteriscoming.data.network.RestService;
import com.space.winteriscoming.data.network.ServiceGenerator;
import com.space.winteriscoming.data.network.dto.Book;
import com.space.winteriscoming.data.network.dto.House;
import com.space.winteriscoming.data.network.dto.Person;
import com.space.winteriscoming.data.storage.models.CharacterEntity;
import com.space.winteriscoming.data.storage.models.CharacterEntityDao;
import com.space.winteriscoming.data.storage.models.DaoSession;

import java.util.ArrayList;
import java.util.List;

import retrofit2.http.Path;
import rx.Observable;

public class DataManager {
    private static final DataManager INSTANCE = new DataManager();
    private final RestService mRestService;
    private final DaoSession mDaoSession;

    private DataManager() {
        this.mRestService = ServiceGenerator.createRxService(RestService.class);
        this.mDaoSession = CustomApp.getDaoSession();
    }

    public static DataManager getInstance() {
        return INSTANCE;
    }

    public RestService getRestService() {
        return mRestService;
    }

    public DaoSession getDaoSession() {
        return mDaoSession;
    }

    //region :::::::::::::::::::::::::::::::::::::::::: DB
    public Observable<List<CharacterEntity>> getCharactersListFromDB(String houseId) {
        List<CharacterEntity> userList = new ArrayList<>();
        try {
            userList = mDaoSession
                    .queryBuilder(CharacterEntity.class)
                    .where(CharacterEntityDao.Properties.MainHouseId.eq(houseId))
                    .orderAsc(CharacterEntityDao.Properties.Name)
                    .build()
                    .list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d("DEV", "getCharactersListFromDB: " + userList.size());
        return Observable.just(userList);
    }

    public Observable<CharacterEntity> getCharacterFromDB(String charId) {
        CharacterEntity characterEntity = new CharacterEntity();
        try {
            characterEntity = mDaoSession
                    .queryBuilder(CharacterEntity.class)
                    .where(CharacterEntityDao.Properties.RemoteId.eq(charId))
                    .build()
                    .unique();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Observable.just(characterEntity);
    }

    public boolean isDBEmpty() {
        return mDaoSession.getHouseEntityDao().count() == 0 || mDaoSession.getCharacterEntityDao().count() == 0;
    }

    public void clearDB() {
        mDaoSession.getHouseEntityDao().deleteAll();
        mDaoSession.getCharacterEntityDao().deleteAll();
    }

    //region :::::::::::::::::::::::::::::::::::::::::: Network
    @RxLogObservable
    public Observable<House> getHouseInfo(@NonNull @Path("houseId") String houseId) {
        return mRestService.getHouseInfo(houseId);
    }

    public Observable<Book> getBookInfo(@NonNull @Path("bookId") String bookId) {
        return mRestService.getBookInfo(bookId);
    }

    public Observable<Person> getCharacterInfo(@NonNull @Path("characterId") String characterId) {
        return mRestService.getCharacterInfo(characterId);
    }
}

