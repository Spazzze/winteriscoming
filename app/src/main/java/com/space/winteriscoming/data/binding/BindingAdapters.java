package com.space.winteriscoming.data.binding;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.space.winteriscoming.R;
import com.space.winteriscoming.common.CustomApp;
import com.space.winteriscoming.utils.Const;

@SuppressWarnings({"unchecked", "unused"})
public class BindingAdapters {

    private static final Context CONTEXT = CustomApp.getContext();
    public static final String TAG = Const.TAG_PREFIX + "BindingAdapters";

    private BindingAdapters() {
        throw new AssertionError();
    }

    @BindingAdapter({"setHouseImage"})
    public static void loadImage(ImageView view, String houseNumber) {
        Drawable image;
        switch (houseNumber) {
            case Const.STARK_ID:
                image = CONTEXT.getResources().getDrawable(R.drawable.house_stark);
                break;
            case Const.LANNISTER_ID:
                image = CONTEXT.getResources().getDrawable(R.drawable.house_lannister);
                break;
            case Const.TARGARYEN_ID:
                image = CONTEXT.getResources().getDrawable(R.drawable.house_targaryen);
                break;
            default:
                image = CONTEXT.getResources().getDrawable(R.drawable.house_unknown);
                break;
        }

        if (image != null) {
            view.setImageDrawable(image);
        }
    }

    @BindingAdapter({"setHouseAvatar"})
    public static void setHouseAvatar(ImageView view, String houseNumber) {
        Drawable image = null;
        switch (houseNumber) {
            case Const.STARK_ID:
                image = CONTEXT.getResources().getDrawable(R.drawable.ic_stark_colored);
                break;
            case Const.LANNISTER_ID:
                image = CONTEXT.getResources().getDrawable(R.drawable.ic_lannister_colored);
                break;
            case Const.TARGARYEN_ID:
                image = CONTEXT.getResources().getDrawable(R.drawable.ic_targaryen_colored);
                break;
        }

        if (image != null) {
            view.setImageDrawable(image);
        }
    }
}


