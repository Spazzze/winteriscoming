package com.space.winteriscoming.data.network.dto;

import com.google.gson.annotations.SerializedName;
import com.space.spacenetworkmodule.data.network.res.BaseResponse;

import java.util.ArrayList;
import java.util.List;

public class Book extends BaseResponse {

    @SerializedName("url")
    private String url;
    @SerializedName("name")
    private String name;
    @SerializedName("isbn")
    private String isbn;
    @SerializedName("authors")
    private List<String> authors = new ArrayList<>();
    @SerializedName("numberOfPages")
    private Integer numberOfPages;
    @SerializedName("publisher")
    private String publisher;
    @SerializedName("country")
    private String country;
    @SerializedName("mediaType")
    private String mediaType;
    @SerializedName("released")
    private String released;
    @SerializedName("characters")
    private List<String> characters = new ArrayList<>();
    @SerializedName("povCharacters")
    private List<String> povCharacters = new ArrayList<>();

    //region Getters
    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public String getIsbn() {
        return isbn;
    }

    public List<String> getAuthors() {
        return authors;
    }

    public Integer getNumberOfPages() {
        return numberOfPages;
    }

    public String getPublisher() {
        return publisher;
    }

    public String getCountry() {
        return country;
    }

    public String getMediaType() {
        return mediaType;
    }

    public String getReleased() {
        return released;
    }

    public List<String> getCharacters() {
        return characters;
    }

    public List<String> getPovCharacters() {
        return povCharacters;
    }
    //endregion

    //region Setters
    public void setUrl(String url) {
        this.url = url;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public void setNumberOfPages(Integer numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public void setReleased(String released) {
        this.released = released;
    }

    public void setCharacters(List<String> characters) {
        this.characters = characters;
    }

    public void setPovCharacters(List<String> povCharacters) {
        this.povCharacters = povCharacters;
    }
    //endregion
}
