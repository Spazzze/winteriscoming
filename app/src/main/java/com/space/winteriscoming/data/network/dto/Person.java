package com.space.winteriscoming.data.network.dto;

import com.google.gson.annotations.SerializedName;
import com.space.spacenetworkmodule.data.network.res.BaseResponse;

import java.util.ArrayList;
import java.util.List;

public class Person extends BaseResponse {

    @SerializedName("url")
    private String url;
    @SerializedName("name")
    private String name;
    @SerializedName("gender")
    private String gender;
    @SerializedName("culture")
    private String culture;
    @SerializedName("born")
    private String born;
    @SerializedName("died")
    private String died;
    @SerializedName("titles")
    private List<String> titles = new ArrayList<>();
    @SerializedName("aliases")
    private List<String> aliases = new ArrayList<>();
    @SerializedName("father")
    private String father;
    @SerializedName("mother")
    private String mother;
    @SerializedName("spouse")
    private String spouse;
    @SerializedName("allegiances")
    private List<String> allegiances = new ArrayList<>();
    @SerializedName("books")
    private List<String> books = new ArrayList<>();
    @SerializedName("povBooks")
    private List<String> povBooks = new ArrayList<>();
    @SerializedName("tvSeries")
    private List<String> tvSeries = new ArrayList<>();
    @SerializedName("playedBy")
    private List<String> playedBy = new ArrayList<>();

    //region Getters
    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String getCulture() {
        return culture;
    }

    public String getBorn() {
        return born;
    }

    public String getDied() {
        return died;
    }

    public List<String> getTitles() {
        return titles;
    }

    public List<String> getAliases() {
        return aliases;
    }

    public String getFather() {
        return father;
    }

    public String getMother() {
        return mother;
    }

    public String getSpouse() {
        return spouse;
    }

    public List<String> getAllegiances() {
        return allegiances;
    }

    public List<String> getBooks() {
        return books;
    }

    public List<String> getPovBooks() {
        return povBooks;
    }

    public List<String> getTvSeries() {
        return tvSeries;
    }

    public List<String> getPlayedBy() {
        return playedBy;
    }
    //endregion

    //region Setters
    public void setUrl(String url) {
        this.url = url;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setCulture(String culture) {
        this.culture = culture;
    }

    public void setBorn(String born) {
        this.born = born;
    }

    public void setDied(String died) {
        this.died = died;
    }

    public void setTitles(List<String> titles) {
        this.titles = titles;
    }

    public void setAliases(List<String> aliases) {
        this.aliases = aliases;
    }

    public void setFather(String father) {
        this.father = father;
    }

    public void setMother(String mother) {
        this.mother = mother;
    }

    public void setSpouse(String spouse) {
        this.spouse = spouse;
    }

    public void setAllegiances(List<String> allegiances) {
        this.allegiances = allegiances;
    }

    public void setBooks(List<String> books) {
        this.books = books;
    }

    public void setPovBooks(List<String> povBooks) {
        this.povBooks = povBooks;
    }

    public void setTvSeries(List<String> tvSeries) {
        this.tvSeries = tvSeries;
    }

    public void setPlayedBy(List<String> playedBy) {
        this.playedBy = playedBy;
    }
    //endregion
}