package com.space.winteriscoming.data.network;

import android.support.annotation.NonNull;

import com.space.winteriscoming.data.network.dto.Book;
import com.space.winteriscoming.data.network.dto.House;
import com.space.winteriscoming.data.network.dto.Person;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

public interface RestService {
    @GET("houses/{houseId}")
    Observable<House> getHouseInfo(@NonNull @Path("houseId") String houseId);

    @GET("books/{bookId}")
    Observable<Book> getBookInfo(@NonNull @Path("bookId") String bookId);

    @GET("characters/{characterId}")
    Observable<Person> getCharacterInfo(@NonNull @Path("characterId") String characterId);
}
