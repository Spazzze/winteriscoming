package com.space.winteriscoming.mvp.presenters.interfaces;

import android.content.Context;
import android.support.annotation.Nullable;

import com.space.winteriscoming.mvp.viewmodels.CharacterViewModel;
import com.space.winteriscoming.mvp.views.ICharActivityView;

/**
 * Created by elena on 25/10/16.
 */

public interface ICharActivityPresenter extends IBasePresenter {
    @Nullable
    ICharActivityView getView();

    CharacterViewModel getViewModel();

    void startCharacterScreenActivity(Context context, CharacterViewModel model);
}
