package com.space.winteriscoming.mvp.presenters.interfaces;

import android.support.annotation.Nullable;

import com.space.winteriscoming.mvp.views.ICharListView;

/**
 * Created by elena on 25/10/16.
 */

public interface ICharListPresenter extends IBasePresenter {
    @Nullable
    ICharListView getView();
}
