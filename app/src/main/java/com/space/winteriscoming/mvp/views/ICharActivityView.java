package com.space.winteriscoming.mvp.views;

import com.space.winteriscoming.mvp.viewmodels.CharacterViewModel;

/**
 * Created by Space on 24.10.2016.
 */

public interface ICharActivityView extends IBaseView {
    void setViewModel(CharacterViewModel model);
}
