package com.space.winteriscoming.mvp.viewmodels;

import android.databinding.BaseObservable;

import com.space.winteriscoming.utils.Const;

public class BaseViewModel extends BaseObservable {

    public final String TAG = Const.TAG_PREFIX + getClass().getSimpleName();
}
