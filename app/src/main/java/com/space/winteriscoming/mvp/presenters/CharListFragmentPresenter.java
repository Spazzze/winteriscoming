package com.space.winteriscoming.mvp.presenters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.space.winteriscoming.mvp.presenters.interfaces.ICharListFragmentPresenter;
import com.space.winteriscoming.mvp.providers.CharListFragmentProvider;
import com.space.winteriscoming.mvp.viewmodels.CharacterViewModel;
import com.space.winteriscoming.mvp.views.ICharListFragmentView;
import com.space.winteriscoming.ui.activities.CharacterScreenActivity;
import com.space.winteriscoming.utils.Const;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by elena on 25/10/16.
 */

public class CharListFragmentPresenter implements ICharListFragmentPresenter {
    private static CharListFragmentPresenter ourInstance = new CharListFragmentPresenter();

    private Map<String, ICharListFragmentView> mViewMap = new HashMap<>();

    private CharListFragmentProvider mProvider;

    public static CharListFragmentPresenter getInstance() {
        return ourInstance;
    }

    private CharListFragmentPresenter() {
        mProvider = new CharListFragmentProvider(this);
    }

    @Override
    public void initView(Bundle savedInstanceState) {

    }

    @Override
    public void dropView(String key) {
        if (mViewMap.containsKey(key)) mViewMap.remove(key);
    }

    @Override
    public void takeView(String key, ICharListFragmentView view) {
        mViewMap.put(key, view);
    }

    @Nullable
    @Override
    public ICharListFragmentView getView(String key) {
        return mViewMap.get(key);
    }

    @Override
    public void startCharacterScreenActivity(Context context, CharacterViewModel model) {
        if (model == null || context == null) return;
        Bundle args = new Bundle();
        args.putParcelable(Const.PARCELABLE_KEY_CHARACTER, new CharacterViewModel().updateValues(model));
        args.putString(Const.PARCELABLE_KEY_CHARACTER_ID, model.getId());
        Intent intent = new Intent(context, CharacterScreenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Const.PARCELABLE_KEY_CHARACTER, args);
        context.startActivity(intent);
    }

    @Override
    public void getData(Bundle bundle) {
        mProvider.provideData(bundle);
    }

    @Override
    public void reportData(String key, List<CharacterViewModel> data) {
        if (mViewMap.get(key) != null) mViewMap.get(key).setupAdapter(data);
    }
}
