package com.space.winteriscoming.mvp.presenters;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.space.winteriscoming.mvp.presenters.interfaces.IBasePresenter;
import com.space.winteriscoming.mvp.viewmodels.BaseViewModel;
import com.space.winteriscoming.mvp.views.IBaseView;

/**
 * Created by Space on 23.10.2016.
 */
public class BasePresenter implements IBasePresenter {

    private static BasePresenter ourInstance = new BasePresenter();
    @Nullable
    private IBaseView mView;
    private BaseViewModel mViewModel;

    public static BasePresenter getInstance() {
        return ourInstance;
    }

    private BasePresenter() {
        mViewModel = new BaseViewModel();
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        if (mView != null) {
            mView.setViewModel(mViewModel);
        }
    }

    @Override
    public void dropView() {
        mView = null;
    }

    @Override
    public void takeView(IBaseView baseView) {
        mView = baseView;
    }

    @Nullable
    @Override
    public IBaseView getView() {
        return mView;
    }

    @Nullable
    @Override
    public BaseViewModel getViewModel() {
        return mViewModel;
    }
}
