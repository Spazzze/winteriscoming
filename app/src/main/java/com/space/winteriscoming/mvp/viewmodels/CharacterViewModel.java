package com.space.winteriscoming.mvp.viewmodels;

import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.space.winteriscoming.BR;
import com.space.winteriscoming.data.managers.DataManager;
import com.space.winteriscoming.data.network.dto.Person;
import com.space.winteriscoming.data.storage.models.CharacterEntity;
import com.space.winteriscoming.utils.AppUtils;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.space.winteriscoming.utils.AppUtils.getCharacterId;

public class CharacterViewModel extends BaseViewModel implements Parcelable {
    private static final DataManager DATA_MANAGER = DataManager.getInstance();

    private String id = "";
    private String name = "";
    private String born = "";
    private String died = "";
    private String words = "";
    private String house = "";
    private String titles = "";
    private String aliases = "";
    private String fatherId = "";
    private String motherId = "";
    private String spouseId = "";
    private String fatherName = "";
    private String motherName = "";
    private String spouseName = "";

    private CharacterViewModel father = null;
    private CharacterViewModel mother = null;
    private CharacterViewModel spouse = null;

    public CharacterViewModel() {
    }

    public CharacterViewModel(CharacterEntity c) {
        if (c == null) return;
        this.id = c.getRemoteId();
        this.name = c.getName();
        this.born = c.getBorn();
        this.died = c.getDied();
        this.words = c.getWords();
        this.house = c.getMainHouseId();
        this.titles = c.getTitles();
        this.aliases = c.getAliases();
        this.fatherId = c.getFather();
        this.motherId = c.getMother();
        this.spouseId = c.getSpouse();
    }

    public CharacterViewModel(Person c, CharacterViewModel spouse) {
        if (c == null) return;
        this.id = getCharacterId(c.getUrl());
        this.name = c.getName();
        this.born = c.getBorn();
        this.died = c.getDied();
        this.titles = TextUtils.join("\n", c.getTitles());
        this.aliases = TextUtils.join("\n", c.getAliases());
        this.fatherId = c.getFather();
        this.motherId = c.getMother();
        this.spouseId = c.getSpouse();
        this.spouse = spouse;
    }

    //region Data management
    public void updateRelations() {
        Log.d(TAG, "updateRelations: ");
        if (father == null && !AppUtils.isEmptyOrNull(fatherId)) updateFatherFromDB();
        if (mother == null && !AppUtils.isEmptyOrNull(motherId)) updateMotherFromDB();
        if (spouse == null && !AppUtils.isEmptyOrNull(spouseId)) updateSpouseFromDB();
    }

    public CharacterViewModel updateValues(@Nullable CharacterViewModel model) {

        if (model == null) return null;

        if (!AppUtils.equals(this.id, model.getId())) {
            setId(model.getId());
        }

        if (!AppUtils.equals(this.name, model.getName())) {
            setName(model.getName());
        }

        if (!AppUtils.equals(this.born, model.getBorn())) {
            setBorn(model.getBorn());
        }

        if (!AppUtils.equals(this.died, model.getDied())) {
            setDied(model.getDied());
        }

        if (!AppUtils.equals(this.words, model.getWords())) {
            setWords(model.getWords());
        }

        if (!AppUtils.equals(this.house, model.getHouse())) {
            setHouse(model.getHouse());
        }

        if (!AppUtils.equals(this.titles, model.getTitles())) {
            setTitles(model.getTitles());
        }

        if (!AppUtils.equals(this.aliases, model.getAliases())) {
            setAliases(model.getAliases());
        }

        if (!AppUtils.equals(this.fatherId, model.getFatherId())) {
            setFatherId(model.getFatherId());
        }

        if (!AppUtils.equals(this.motherId, model.getMotherId())) {
            setMotherId(model.getMotherId());
        }

        if (!AppUtils.equals(this.spouseId, model.getSpouseId())) {
            setSpouseId(model.getSpouseId());
        }

        if (!AppUtils.equals(this.fatherName, model.getFatherName())) {
            setFatherName(model.getFatherName());
        }

        if (!AppUtils.equals(this.motherName, model.getMotherName())) {
            setMotherName(model.getMotherName());
        }

        if (!AppUtils.equals(this.spouseName, model.getSpouseName())) {
            setSpouseName(model.getSpouseName());
        }

        if (!AppUtils.equals(this.father, model.getFather())) {
            setFather(model.getFather());
        }

        if (!AppUtils.equals(this.mother, model.getMother())) {
            setMother(model.getMother());
        }

        if (!AppUtils.equals(this.spouse, model.getSpouse())) {
            setSpouse(model.getSpouse());
        }
        return this;
    }

    private void updateFatherFromDB() {
        Log.d(TAG, "updateFatherFromDB: ");
        Observable.just(fatherId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.newThread())
                .flatMap(DATA_MANAGER::getCharacterFromDB)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        l -> setFather(new CharacterViewModel(l)),
                        t -> Log.e(TAG, "updateFatherFromDB: ", t));
    }

    private void updateMotherFromDB() {
        Log.d(TAG, "updateMotherFromDB: ");
        Observable.just(motherId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.newThread())
                .flatMap(DATA_MANAGER::getCharacterFromDB)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        l -> setMother(new CharacterViewModel(l)),
                        t -> Log.e(TAG, "updateMotherFromDB: ", t));
    }

    private void updateSpouseFromDB() {
        Log.d(TAG, "updateSpouseFromDB: ");
        Observable.just(spouseId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.newThread())
                .flatMap(DATA_MANAGER::getCharacterFromDB)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        this::handleDBResponse,
                        t -> Log.e(TAG, "updateSpouseFromDB: ", t));
    }

    private void handleDBResponse(CharacterEntity characterEntity) {
        if (characterEntity != null) setSpouse(new CharacterViewModel(characterEntity));
        else updateSpouseFromNetwork();
    }

    private void updateSpouseFromNetwork() {
        Log.d(TAG, "updateSpouseFromNetwork: ");
        Observable.just(spouseId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.newThread())
                .flatMap(DATA_MANAGER::getCharacterInfo)
                .subscribe(
                        this::handleResponse,
                        t -> Log.e(TAG, "updateSpouseFromNetwork: ", t));
    }

    private void handleResponse(Person c) {
        if (c != null) {
            setSpouse(new CharacterViewModel(c, new CharacterViewModel().updateValues(this)));
            Observable.just(new CharacterEntity(c))
                    .subscribeOn(Schedulers.newThread())
                    .doOnNext(a -> DATA_MANAGER.getDaoSession().getCharacterEntityDao().insertOrReplace(a))
                    .subscribe();
        }
    }
    //endregion

    //region Getters
    @Bindable
    public String getFatherName() {
        return fatherName;
    }

    @Bindable
    public String getMotherName() {
        return motherName;
    }

    @Bindable
    public String getSpouseName() {
        return spouseName;
    }

    @Bindable
    public String getId() {
        return id;
    }

    @Bindable
    public String getHouse() {
        return house;
    }

    @Bindable
    public String getName() {
        return name;
    }

    @Bindable
    public String getBorn() {
        return born;
    }

    @Bindable
    public String getDied() {
        return died;
    }

    @Bindable
    public String getWords() {
        return words;
    }

    @Bindable
    public String getTitles() {
        return titles;
    }

    @Bindable
    public String getAliases() {
        return aliases;
    }

    @Bindable
    public String getFatherId() {
        return fatherId;
    }

    @Bindable
    public String getMotherId() {
        return motherId;
    }

    @Bindable
    public String getSpouseId() {
        return spouseId;
    }

    @Bindable
    public CharacterViewModel getFather() {
        return father;
    }

    @Bindable
    public CharacterViewModel getMother() {
        return mother;
    }

    @Bindable
    public CharacterViewModel getSpouse() {
        return spouse;
    }
    //endregion

    //region Setters

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
        notifyPropertyChanged(BR.fatherName);
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
        notifyPropertyChanged(BR.motherName);
    }

    public void setSpouseName(String spouseName) {
        this.spouseName = spouseName;
        notifyPropertyChanged(BR.spouseName);
    }

    public void setId(String id) {
        this.id = id;
        notifyPropertyChanged(BR.id);
    }

    public void setHouse(String house) {
        this.house = house;
        notifyPropertyChanged(BR.house);
    }

    public void setName(String name) {
        this.name = name;
        notifyPropertyChanged(BR.name);
    }

    public void setBorn(String born) {
        this.born = born;
        notifyPropertyChanged(BR.born);
    }

    public void setDied(String died) {
        this.died = died;
        notifyPropertyChanged(BR.died);
    }

    public void setWords(String words) {
        this.words = words;
        notifyPropertyChanged(BR.words);
    }

    public void setTitles(String titles) {

        this.titles = titles;
        notifyPropertyChanged(BR.titles);
    }

    public void setAliases(String aliases) {

        this.aliases = aliases;
        notifyPropertyChanged(BR.aliases);
    }

    public void setFatherId(String fatherId) {
        this.fatherId = fatherId;
        notifyPropertyChanged(BR.fatherId);
    }

    public void setMotherId(String motherId) {
        this.motherId = motherId;
        notifyPropertyChanged(BR.motherId);
    }

    public void setSpouseId(String spouseId) {
        this.spouseId = spouseId;
        notifyPropertyChanged(BR.spouseId);
    }

    public void setFather(CharacterViewModel father) {
        this.father = father;
        if (father != null) {
            this.fatherName = father.getName();
            notifyPropertyChanged(BR.father);
            notifyPropertyChanged(BR.fatherName);
        }
    }

    public void setMother(CharacterViewModel mother) {
        this.mother = mother;
        if (mother != null) {
            this.motherName = mother.getName();
            notifyPropertyChanged(BR.mother);
            notifyPropertyChanged(BR.motherName);
        }
    }

    public void setSpouse(CharacterViewModel spouse) {
        this.spouse = spouse;
        if (spouse != null) {
            this.spouseName = spouse.getName();
            notifyPropertyChanged(BR.spouse);
            notifyPropertyChanged(BR.spouseName);
        }
    }

    //endregion

    //region Parcel
    protected CharacterViewModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        born = in.readString();
        died = in.readString();
        words = in.readString();
        house = in.readString();
        titles = in.readString();
        aliases = in.readString();
        fatherId = in.readString();
        motherId = in.readString();
        spouseId = in.readString();
        fatherName = in.readString();
        motherName = in.readString();
        spouseName = in.readString();
        father = (CharacterViewModel) in.readValue(CharacterViewModel.class.getClassLoader());
        mother = (CharacterViewModel) in.readValue(CharacterViewModel.class.getClassLoader());
        spouse = (CharacterViewModel) in.readValue(CharacterViewModel.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(born);
        dest.writeString(died);
        dest.writeString(words);
        dest.writeString(house);
        dest.writeString(titles);
        dest.writeString(aliases);
        dest.writeString(fatherId);
        dest.writeString(motherId);
        dest.writeString(spouseId);
        dest.writeString(fatherName);
        dest.writeString(motherName);
        dest.writeString(spouseName);
        dest.writeValue(father);
        dest.writeValue(mother);
        dest.writeValue(spouse);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CharacterViewModel> CREATOR = new Parcelable.Creator<CharacterViewModel>() {
        @Override
        public CharacterViewModel createFromParcel(Parcel in) {
            return new CharacterViewModel(in);
        }

        @Override
        public CharacterViewModel[] newArray(int size) {
            return new CharacterViewModel[size];
        }
    };
    //endregion

    @Override
    public String toString() {
        return "CharacterViewModel{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", born='" + born + '\'' +
                ", died='" + died + '\'' +
                ", words='" + words + '\'' +
                ", house='" + house + '\'' +
                ", titles='" + titles + '\'' +
                ", aliases='" + aliases + '\'' +
                ", fatherId='" + fatherId + '\'' +
                ", motherId='" + motherId + '\'' +
                ", spouseId='" + spouseId + '\'' +
                '}';
    }
}