package com.space.winteriscoming.mvp.presenters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;

import com.space.winteriscoming.mvp.providers.SplashProvider;
import com.space.winteriscoming.mvp.presenters.interfaces.ISplashPresenter;
import com.space.winteriscoming.mvp.viewmodels.BaseViewModel;
import com.space.winteriscoming.mvp.views.IBaseView;
import com.space.winteriscoming.mvp.views.ISplashView;
import com.space.winteriscoming.ui.activities.MainActivity;
import com.space.winteriscoming.ui.fragments.NetworkFragment;
import com.space.winteriscoming.utils.AppConfig;

import java.util.Date;

/**
 * Created by Space on 24.10.2016.
 */

public class SplashPresenter implements ISplashPresenter {
    private static SplashPresenter ourInstance = new SplashPresenter();
    @Nullable
    private ISplashView mView;
    private NetworkFragment mNetworkFragment;
    private SplashProvider mProvider;
    private long mTime;

    public static SplashPresenter getInstance() {
        return ourInstance;
    }

    private SplashPresenter() {
        mNetworkFragment = new NetworkFragment();
        mProvider = new SplashProvider();
        mTime = new Date().getTime();
    }

    @Override
    public void initView(Bundle savedInstanceState) {
    }

    @Override
    public void dropView() {
        mView = null;
    }

    @Override
    public <T extends IBaseView> void takeView(T baseView) {
        if (baseView instanceof ISplashView) mView = (ISplashView) baseView;
    }

    @Nullable
    @Override
    public ISplashView getView() {
        return mView;
    }

    @Nullable
    @Override
    public BaseViewModel getViewModel() {
        return null;
    }

    @Override
    public void startMainActivity(Context context) {
        new Handler().postDelayed(() -> {
            if (mView == null) return;
            Intent intent = new Intent(context, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
            if (mView instanceof Activity) ((Activity) mView).finish();
        }, AppConfig.SPLASH_FADE_DELAY - (new Date().getTime() - mTime));
    }

    @Override
    public void startDownload() {
        Log.d("DEV ", "startDownload: ");
        if (mView != null) {
            if (mProvider.isDbDownloadNeeded()) mView.attachNetworkFragment(mNetworkFragment);
            else {
                mView.endLoad();
            }
        }
    }
}
