package com.space.winteriscoming.mvp.providers.interfaces;

import android.os.Bundle;
import android.support.annotation.NonNull;

/**
 * Created by elena on 25/10/16.
 */

public interface ICharListFragmentProvider {
    void provideData(@NonNull Bundle bundle);
}
