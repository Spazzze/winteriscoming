package com.space.winteriscoming.mvp.views;

import com.space.winteriscoming.ui.fragments.NetworkFragment;

/**
 * Created by Space on 24.10.2016.
 */

public interface ISplashView extends IBaseView {
    void increaseProgress();

    void showProgress(int max);

    void endLoad();

    void attachNetworkFragment(NetworkFragment fragment);
}
