package com.space.winteriscoming.mvp.presenters.interfaces;

import android.content.Context;
import android.support.annotation.Nullable;

import com.space.winteriscoming.mvp.views.ISplashView;

/**
 * Created by Space on 24.10.2016.
 */

public interface ISplashPresenter extends IBasePresenter {

    @Nullable
    ISplashView getView();

    void startMainActivity(Context context);

    void startDownload();
}
