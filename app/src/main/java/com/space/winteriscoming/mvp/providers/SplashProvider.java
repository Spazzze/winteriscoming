package com.space.winteriscoming.mvp.providers;

import com.space.winteriscoming.data.managers.DataManager;
import com.space.winteriscoming.mvp.providers.interfaces.ISplashProvider;
import com.space.winteriscoming.utils.AppUtils;

/**
 * Created by Space on 25.10.2016.
 */

public class SplashProvider implements ISplashProvider {

    public SplashProvider() {
    }

    public boolean isDbDownloadNeeded() {
        return AppUtils.isNetworkAvailable() && DataManager.getInstance().isDBEmpty();
    }
}
