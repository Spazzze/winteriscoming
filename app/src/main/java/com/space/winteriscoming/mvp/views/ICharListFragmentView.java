package com.space.winteriscoming.mvp.views;

import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import com.space.winteriscoming.mvp.viewmodels.CharacterViewModel;

import java.util.List;

/**
 * Created by elena on 25/10/16.
 */

public interface ICharListFragmentView {
    void showMessage(String message);

    void showMessage(@StringRes int messageId);

    void showError(Throwable error);

    void showLoad();

    void hideLoad();

    void setupAdapter(@NonNull List<CharacterViewModel> list);
}
