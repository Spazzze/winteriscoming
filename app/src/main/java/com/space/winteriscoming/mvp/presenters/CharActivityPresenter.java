package com.space.winteriscoming.mvp.presenters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.space.winteriscoming.mvp.presenters.interfaces.ICharActivityPresenter;
import com.space.winteriscoming.mvp.viewmodels.CharacterViewModel;
import com.space.winteriscoming.mvp.views.IBaseView;
import com.space.winteriscoming.mvp.views.ICharActivityView;
import com.space.winteriscoming.ui.activities.CharacterScreenActivity;
import com.space.winteriscoming.utils.Const;

/**
 * Created by elena on 25/10/16.
 */

public class CharActivityPresenter implements ICharActivityPresenter {

    private static CharActivityPresenter ourInstance = new CharActivityPresenter();
    @Nullable
    private ICharActivityView mView;

    private CharacterViewModel mViewModel;

    public static CharActivityPresenter getInstance() {
        return ourInstance;
    }

    private CharActivityPresenter() {
        mViewModel = new CharacterViewModel();
    }

    @Override
    public void initView(@NonNull Bundle bundle) {
        CharacterViewModel savedModel = bundle.getParcelable(Const.PARCELABLE_KEY_CHARACTER);
        mViewModel.updateValues(savedModel);
        mViewModel.updateRelations();
        if (mView != null) {
            mView.setViewModel(mViewModel);
            if (!mViewModel.getDied().isEmpty())
                mView.showMessage("Died at: " + mViewModel.getDied());
        }
    }

    @Override
    public void dropView() {
        mView = null;
    }

    @Override
    public <T extends IBaseView> void takeView(T view) {
        if (view instanceof ICharActivityView) mView = (ICharActivityView) view;
    }

    @Nullable
    @Override
    public ICharActivityView getView() {
        return mView;
    }

    @Override
    public CharacterViewModel getViewModel() {
        return mViewModel;
    }

    public void startCharacterScreenActivity(Context context, CharacterViewModel model) {
        if (model == null || mView == null) return;
        Bundle args = new Bundle();
        args.putParcelable(Const.PARCELABLE_KEY_CHARACTER, new CharacterViewModel().updateValues(model));
        args.putString(Const.PARCELABLE_KEY_CHARACTER_ID, model.getId());
        Intent intent = new Intent(context, CharacterScreenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Const.PARCELABLE_KEY_CHARACTER, args);
        context.startActivity(intent);
    }
}
