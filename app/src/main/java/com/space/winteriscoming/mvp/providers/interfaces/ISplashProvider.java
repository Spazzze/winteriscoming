package com.space.winteriscoming.mvp.providers.interfaces;

/**
 * Created by Space on 25.10.2016.
 */

public interface ISplashProvider {
    boolean isDbDownloadNeeded();
}
