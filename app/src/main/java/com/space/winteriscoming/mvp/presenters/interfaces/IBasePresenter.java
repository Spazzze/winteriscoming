package com.space.winteriscoming.mvp.presenters.interfaces;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.space.winteriscoming.mvp.viewmodels.BaseViewModel;
import com.space.winteriscoming.mvp.views.IBaseView;

/**
 * Created by Space on 23.10.2016.
 */

public interface IBasePresenter {

    void initView(Bundle savedInstanceState);

    void dropView();

    <T extends IBaseView> void takeView(T view);

    @Nullable
    IBaseView getView();

    BaseViewModel getViewModel();
}
