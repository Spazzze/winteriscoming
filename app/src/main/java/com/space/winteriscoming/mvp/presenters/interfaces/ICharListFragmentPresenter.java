package com.space.winteriscoming.mvp.presenters.interfaces;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.space.winteriscoming.mvp.viewmodels.CharacterViewModel;
import com.space.winteriscoming.mvp.views.ICharListFragmentView;

import java.util.List;

/**
 * Created by elena on 25/10/16.
 */

public interface ICharListFragmentPresenter {

    void initView(Bundle savedInstanceState);

    void dropView(String key);

    void takeView(String key, ICharListFragmentView view);

    @Nullable
    ICharListFragmentView getView(String key);

    void startCharacterScreenActivity(Context context, CharacterViewModel model);

    void getData(Bundle savedInstanceState);

    void reportData(String key, List<CharacterViewModel> data);
}
