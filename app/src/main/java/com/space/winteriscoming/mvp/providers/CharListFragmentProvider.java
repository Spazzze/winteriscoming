package com.space.winteriscoming.mvp.providers;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.space.winteriscoming.data.managers.DataManager;
import com.space.winteriscoming.data.storage.models.CharacterEntity;
import com.space.winteriscoming.mvp.presenters.CharListFragmentPresenter;
import com.space.winteriscoming.mvp.providers.interfaces.ICharListFragmentProvider;
import com.space.winteriscoming.mvp.viewmodels.CharacterViewModel;
import com.space.winteriscoming.utils.Const;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by elena on 25/10/16.
 */

public class CharListFragmentProvider implements ICharListFragmentProvider {
    public final String TAG = Const.TAG_PREFIX + getClass().getSimpleName();

    private static final DataManager DATA_MANAGER = DataManager.getInstance();
    private CharListFragmentPresenter mPresenter;

    public CharListFragmentProvider(CharListFragmentPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void provideData(@NonNull Bundle bundle) {

        String fragmentHouseID = bundle.getString(Const.PARCELABLE_KEY_HOUSEID);
        List<CharacterViewModel> savedList = bundle.getParcelableArrayList(Const.PARCELABLE_KEY_CHARACTER_LIST);

        if (savedList != null) {
            mPresenter.reportData(fragmentHouseID, savedList);
        } else if (fragmentHouseID != null) {
            requestDataFromDB(fragmentHouseID);
        } else mPresenter.reportData(fragmentHouseID, null);
    }

    private void requestDataFromDB(@NonNull String houseID) {
        Observable.just(houseID)
                .subscribeOn(Schedulers.newThread())
                .flatMap(DATA_MANAGER::getCharactersListFromDB)
                .map(this::convertDataFromDB)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(out -> mPresenter.reportData(houseID, out),
                        t -> Log.e(TAG, "requestListFromDB: ", t));
    }

    private List<CharacterViewModel> convertDataFromDB(List<CharacterEntity> list) {
        Log.e(TAG, "convertDataFromDB: " + list.size());
        return new ArrayList<CharacterViewModel>() {{
            for (CharacterEntity e : list) {
                add(new CharacterViewModel(e));
            }
        }};
    }
}
