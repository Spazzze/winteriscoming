package com.space.winteriscoming.mvp.presenters;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.space.winteriscoming.mvp.presenters.interfaces.ICharListPresenter;
import com.space.winteriscoming.mvp.viewmodels.BaseViewModel;
import com.space.winteriscoming.mvp.views.IBaseView;
import com.space.winteriscoming.mvp.views.ICharListView;

/**
 * Created by elena on 25/10/16.
 */

public class CharListPresenter implements ICharListPresenter {

    private static CharListPresenter ourInstance = new CharListPresenter();
    @Nullable
    private ICharListView mView;

    public static CharListPresenter getInstance() {
        return ourInstance;
    }

    private CharListPresenter() {
    }

    @Override
    public void initView(Bundle savedInstanceState) {

    }

    @Override
    public void dropView() {
        mView = null;
    }

    @Override
    public <T extends IBaseView> void takeView(T view) {
        if (view instanceof ICharListView) mView = (ICharListView) view;
    }

    @Nullable
    @Override
    public ICharListView getView() {
        return mView;
    }

    @Override
    public BaseViewModel getViewModel() {
        return null;
    }
}
