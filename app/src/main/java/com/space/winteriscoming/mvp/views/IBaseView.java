package com.space.winteriscoming.mvp.views;

import android.support.annotation.StringRes;

import com.space.winteriscoming.mvp.presenters.interfaces.IBasePresenter;
import com.space.winteriscoming.mvp.viewmodels.BaseViewModel;

/**
 * Created by Space on 23.10.2016.
 */

public interface IBaseView {

    void showMessage(String message);

    void showMessage(@StringRes int messageId);

    void showError(Throwable error);

    void showLoad();

    void hideLoad();

    IBasePresenter getPresenter();

    void setViewModel(BaseViewModel model);
}
