package com.space.winteriscoming.ui.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.space.winteriscoming.R;
import com.space.winteriscoming.common.CustomApp;
import com.space.winteriscoming.ui.fragments.CharacterListFragment;
import com.space.winteriscoming.utils.Const;

public class SlidingPagerAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public SlidingPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        String houseId = "0";
        switch (position) {
            case 0:
                houseId = Const.STARK_ID;
                break;
            case 1:
                houseId = Const.LANNISTER_ID;
                break;
            case 2:
                houseId = Const.TARGARYEN_ID;
                break;
        }
        CharacterListFragment fragment = new CharacterListFragment();
        Bundle b = new Bundle();
        b.putString(Const.PARCELABLE_KEY_HOUSEID, houseId);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return CustomApp.getContext().getString(R.string.header_stark);
            case 1:
                return CustomApp.getContext().getString(R.string.header_lannister);
            case 2:
                return CustomApp.getContext().getString(R.string.header_targaryen);
            default:
                return "";
        }
    }
}