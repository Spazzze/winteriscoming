package com.space.winteriscoming.ui.activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.space.winteriscoming.R;
import com.space.winteriscoming.mvp.presenters.BasePresenter;
import com.space.winteriscoming.mvp.presenters.interfaces.IBasePresenter;
import com.space.winteriscoming.mvp.viewmodels.BaseViewModel;
import com.space.winteriscoming.mvp.views.IBaseView;
import com.space.winteriscoming.utils.Const;

public class BaseActivity extends AppCompatActivity implements IBaseView {

    public final String TAG = Const.TAG_PREFIX + getClass().getSimpleName();
    private ProgressDialog mProgressDialog;
    private BasePresenter mPresenter = BasePresenter.getInstance();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        mPresenter.takeView(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void showLoad() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this, R.style.custom_dialog);
            mProgressDialog.setCancelable(false);
            if (mProgressDialog.getWindow() != null)
                mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        mProgressDialog.show();
        mProgressDialog.setContentView(R.layout.item_progress_splash);
    }

    @Override
    public void hideLoad() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public IBasePresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showMessage(@StringRes int messageId) {
        Toast.makeText(this, getString(messageId), Toast.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable error) {
        Toast.makeText(this, error.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void setViewModel(BaseViewModel model) {

    }
}
