package com.space.winteriscoming.ui.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;

import com.space.winteriscoming.R;
import com.space.winteriscoming.databinding.ActivityMainBinding;
import com.space.winteriscoming.mvp.presenters.CharListPresenter;
import com.space.winteriscoming.mvp.views.ICharListView;
import com.space.winteriscoming.ui.adapters.SlidingPagerAdapter;
import com.space.winteriscoming.utils.AppConfig;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, ICharListView {

    private ActivityMainBinding mBinding;

    private CharListPresenter mPresenter = CharListPresenter.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        DataBindingUtil.bind(mBinding.navView.getHeaderView(0));

        setupToolbar();
        setupViewPager();
        mBinding.navView.setNavigationItemSelectedListener(this);
    }

    private void setupViewPager() {
        TabLayout tabLayout = mBinding.tabLayout;

        final SlidingPagerAdapter adapter = new SlidingPagerAdapter(getSupportFragmentManager(), AppConfig.VIEWPAGER_PAGE_COUNT);
        mBinding.pager.setAdapter(adapter);

        for (int i = 0; i < adapter.getCount(); i++) {
            tabLayout.addTab(tabLayout.newTab().setText(adapter.getPageTitle(i)));
        }
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setupWithViewPager(mBinding.pager);

        mBinding.pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mBinding.pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                mBinding.pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void setupToolbar() {
        setSupportActionBar(mBinding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mBinding.drawer.openDrawer(GravityCompat.START);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navMenu_stark:
                mBinding.pager.setCurrentItem(0);
                break;
            case R.id.navMenu_lannister:
                mBinding.pager.setCurrentItem(1);
                break;
            case R.id.navMenu_targaryen:
                mBinding.pager.setCurrentItem(2);
                break;
        }
        mBinding.drawer.closeDrawer(GravityCompat.START);
        return false;
    }
}
