package com.space.winteriscoming.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.space.spacenetworkmodule.data.network.NetworkRequestID;
import com.space.spacenetworkmodule.data.network.SpcNetworkRequest;
import com.space.spacenetworkmodule.retain_fragments.BaseNetworkFragment;
import com.space.winteriscoming.data.managers.DataManager;
import com.space.winteriscoming.data.network.dto.House;
import com.space.winteriscoming.data.network.dto.Person;
import com.space.winteriscoming.data.storage.models.CharacterEntity;
import com.space.winteriscoming.data.storage.models.HouseEntity;
import com.space.winteriscoming.mvp.views.ISplashView;
import com.space.winteriscoming.utils.Const;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.space.winteriscoming.utils.AppUtils.getCharacterId;
import static com.space.winteriscoming.utils.AppUtils.getHouseId;

public class NetworkFragment extends BaseNetworkFragment {

    private static final DataManager DATA_MANAGER = DataManager.getInstance();

    private ISplashView mActivityCallbacks;

    private Map<String, List<String>> mMemberIdsMap = new HashMap<>();
    private Map<String, String> mHouseWordsMap = new HashMap<>();
    private volatile Set<String> mSearchedUserIdsList = new HashSet<>();

    private String mMaxCharactersHouseId = "";
    private int mCompleteCount = 0;
    private SpcNetworkRequest mAllCharactersReq = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getAllHousesInfo();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof ISplashView) {
            mActivityCallbacks = (ISplashView) activity;
        } else {
            throw new IllegalStateException("Parent activity must implement ISplashView");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivityCallbacks = null;
    }

    public void getAllHousesInfo() {
        Log.d(TAG, "getAllHousesInfo: ");

        resetFields();

        SpcNetworkRequest req = new SpcNetworkRequest(NetworkRequestID.HOUSES);
        onRequestStarted(req);

        Observable.from(Const.HOUSES_IDS)
                .subscribeOn(Schedulers.newThread())
                .flatMap(DATA_MANAGER::getHouseInfo)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        this::handleHouseResponse,
                        throwable -> onRequestFailure(req, throwable),
                        () -> onHousesReqComplete(req));
    }

    private void resetFields() {
        mAllCharactersReq = null;
        mMemberIdsMap.clear();
        mSearchedUserIdsList.clear();
        mMaxCharactersHouseId = "";
        mCompleteCount = 0;
        for (String id : Const.HOUSES_IDS) {
            mMemberIdsMap.put(id, new ArrayList<>());
        }
    }

    private void handleHouseResponse(House h) {
        DATA_MANAGER.getDaoSession().getHouseEntityDao().insertOrReplace(new HouseEntity(h));

        String houseId = getHouseId(h.getUrl());
        Log.d(TAG, "handleHouseResponse: " + houseId);
        mHouseWordsMap.put(houseId, h.getWords());

        for (String m : h.getSwornMembers()) {
            String memberId = getCharacterId(m);
            if (!memberId.isEmpty()) mMemberIdsMap.get(houseId).add(memberId);
        }
    }

    private void onHousesReqComplete(SpcNetworkRequest req) {
        onRequestComplete(req);
        getAllCharacters();
    }

    private void getAllCharacters() {
        mAllCharactersReq = new SpcNetworkRequest(NetworkRequestID.ALL_CHARACTERS);
        onRequestStarted(mAllCharactersReq);

        int max = 0;
        for (Map.Entry<String, List<String>> pair : mMemberIdsMap.entrySet()) {
            if (pair.getValue().size() > max) {
                max = pair.getValue().size();
                mMaxCharactersHouseId = pair.getKey();
            }
        }
        if (mActivityCallbacks != null) mActivityCallbacks.showProgress(max);

        for (Map.Entry<String, List<String>> pair : mMemberIdsMap.entrySet()) {
            getCharacters(pair.getKey(), pair.getValue());
        }
    }

    private void getCharacters(String houseId, List<String> list) {
        SpcNetworkRequest req = new SpcNetworkRequest(NetworkRequestID.CHARACTERS, houseId);
        onRequestStarted(req);

        Observable.from(list)
                .subscribeOn(Schedulers.newThread())
                .flatMap(this::getCharacterInfo)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext((p) -> increaseProgress(houseId))
                .subscribe(
                        c -> handleCharacterResponse(c, houseId),
                        throwable -> onRequestFailure(req, throwable),
                        () -> onCharactersReqComplete(req));
    }

    private Observable<Person> getCharacterInfo(String id) {
        if (mSearchedUserIdsList.contains(id)) return null;
        mSearchedUserIdsList.add(id);
        return DATA_MANAGER.getCharacterInfo(id);
    }

    private void increaseProgress(String houseId) {
        if (houseId.equals(mMaxCharactersHouseId) && mActivityCallbacks != null)
            mActivityCallbacks.increaseProgress();
    }

    private void handleCharacterResponse(Person c, String houseId) {
        if (c != null)
            DATA_MANAGER.getDaoSession().getCharacterEntityDao().insertOrReplace(new CharacterEntity(c, houseId, mHouseWordsMap.get(houseId)));
    }

    private void onCharactersReqComplete(SpcNetworkRequest req) {
        onRequestComplete(req);
        mCompleteCount++;
        if (mCompleteCount == Const.HOUSES_IDS.size()) onRequestComplete(mAllCharactersReq);
    }
}
