package com.space.winteriscoming.ui.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;

import com.space.winteriscoming.R;
import com.space.winteriscoming.databinding.ActivityCharacterScreenBinding;
import com.space.winteriscoming.mvp.presenters.CharActivityPresenter;
import com.space.winteriscoming.mvp.viewmodels.CharacterViewModel;
import com.space.winteriscoming.mvp.views.ICharActivityView;
import com.space.winteriscoming.utils.Const;

public class CharacterScreenActivity extends BaseActivity implements View.OnClickListener, ICharActivityView {

    CharActivityPresenter mPresenter = CharActivityPresenter.getInstance();
    ActivityCharacterScreenBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_character_screen);
        mPresenter.takeView(this);
        Bundle extra;
        if (getIntent().getExtras() != null && ((extra = getIntent().getExtras().getBundle(Const.PARCELABLE_KEY_CHARACTER)) != null)) {
            mPresenter.initView(extra);
        } else if (savedInstanceState != null) {
            mPresenter.initView(savedInstanceState);
        } else {
            showMessage(getString(R.string.error_something_went_wrong));
        }

        setupToolbar();
        mBinding.contentCharacterLayout.fatherBtn.setOnClickListener(this);
        mBinding.contentCharacterLayout.motherBtn.setOnClickListener(this);
        mBinding.contentCharacterLayout.spouseBtn.setOnClickListener(this);
    }

    @Override
    public void setViewModel(CharacterViewModel model) {
        mBinding.setPerson(model);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (outState == null) outState = new Bundle();
        outState.putParcelable(Const.PARCELABLE_KEY_CHARACTER, mPresenter.getViewModel());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(mBinding.coordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable error) {
        showMessage(error.getMessage());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.father_btn:
                mPresenter.startCharacterScreenActivity(this, mPresenter.getViewModel().getFather());
                break;
            case R.id.mother_btn:
                mPresenter.startCharacterScreenActivity(this, mPresenter.getViewModel().getMother());
                break;
            case R.id.spouse_btn:
                mPresenter.startCharacterScreenActivity(this, mPresenter.getViewModel().getSpouse());
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupToolbar() {
        setSupportActionBar(mBinding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}
