package com.space.winteriscoming.ui.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.space.spacenetworkmodule.data.network.BaseNetworkTaskCallbacks;
import com.space.spacenetworkmodule.data.network.SpcNetworkRequest;
import com.space.winteriscoming.R;
import com.space.winteriscoming.databinding.ActivitySplashBinding;
import com.space.winteriscoming.mvp.presenters.SplashPresenter;
import com.space.winteriscoming.mvp.views.ISplashView;
import com.space.winteriscoming.ui.fragments.NetworkFragment;
import com.space.winteriscoming.ui.view.CustomProgressView;
import com.space.winteriscoming.utils.Const;

public class SplashActivity extends BaseActivity implements BaseNetworkTaskCallbacks, ISplashView {

    private static final String TAG = Const.TAG_PREFIX + "Splash Activity";

    private SplashPresenter mPresenter = SplashPresenter.getInstance();

    private ActivitySplashBinding mBinding;
    private CustomProgressView mProgressView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        mProgressView = mBinding.progress;
        mPresenter.takeView(this);
        mPresenter.startDownload();
    }

    public void showProgress(int max) {
        mBinding.avi.show();
        mProgressView.setMax(max);
        mProgressView.setCurProgress(0);
    }

    public void increaseProgress() {
        if (mProgressView != null)
            mProgressView.setCurProgress(mProgressView.getCurProgress() + 1);
    }

    public void endLoad() {
        if (mProgressView != null)
            mProgressView.setCurProgress(mProgressView.getMax());
        mPresenter.startMainActivity(this);
    }

    @Override
    public void attachNetworkFragment(NetworkFragment fragment) {
        getSupportFragmentManager().beginTransaction().add(fragment, NetworkFragment.class.getName()).commit();
    }

    @Override
    public void onNetworkRequestStarted(@NonNull SpcNetworkRequest request) {
        Log.d(TAG, "onNetworkRequestStarted: " + request.getId());
    }

    @Override
    public void onNetworkRequestFinished(@NonNull SpcNetworkRequest request) {
        Log.d(TAG, "onNetworkRequestFinished: " + request.getId());
        switch (request.getId()) {
            case ALL_CHARACTERS:
                mPresenter.startMainActivity(this);
                break;
        }
    }

    @Override
    public void onNetworkRequestFailed(@NonNull SpcNetworkRequest request) {
        Log.e(TAG, "onNetworkRequestFailed: " + request.getError());
    }
}