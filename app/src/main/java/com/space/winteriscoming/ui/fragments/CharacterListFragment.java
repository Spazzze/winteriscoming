package com.space.winteriscoming.ui.fragments;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.space.winteriscoming.BR;
import com.space.winteriscoming.R;
import com.space.winteriscoming.databinding.FragmentCharacterListBinding;
import com.space.winteriscoming.mvp.presenters.CharListFragmentPresenter;
import com.space.winteriscoming.mvp.viewmodels.CharacterViewModel;
import com.space.winteriscoming.mvp.views.IBaseView;
import com.space.winteriscoming.mvp.views.ICharListFragmentView;
import com.space.winteriscoming.ui.adapters.RecyclerBindingAdapter;
import com.space.winteriscoming.utils.Const;

import java.util.ArrayList;
import java.util.List;

public class CharacterListFragment extends Fragment implements ICharListFragmentView {

    public final String TAG = Const.TAG_PREFIX + getClass().getSimpleName();

    private FragmentCharacterListBinding mBinding;
    private RecyclerBindingAdapter<CharacterViewModel> mAdapter;
    private CharListFragmentPresenter mPresenter = CharListFragmentPresenter.getInstance();

    public IBaseView mCallbacks;

    private String mFragmentHouseID = "";
    //region :::::::::::::::::::::::::::::::::::::::::: Life cycle
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(TAG, "onAttach: ");
        if (activity instanceof IBaseView) {
            mCallbacks = (IBaseView) activity;
        } else {
            throw new IllegalStateException("Parent activity must implement IBaseView");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_character_list, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.e(TAG, "onViewCreated: " + (mPresenter == null));
        mFragmentHouseID = getArguments().getString(Const.PARCELABLE_KEY_HOUSEID);
        mPresenter.takeView(mFragmentHouseID, this);

        initRecycleView();

        if (savedInstanceState != null) mPresenter.getData(savedInstanceState);
        else if (getArguments() != null) mPresenter.getData(getArguments());
        else showMessage(R.string.error_something_went_wrong);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mAdapter != null) {
            if (outState == null) outState = new Bundle();
            outState.putParcelableArrayList(Const.PARCELABLE_KEY_CHARACTER,
                    (ArrayList<? extends Parcelable>) mAdapter.getItems());
            outState.putString(Const.PARCELABLE_KEY_HOUSEID, mFragmentHouseID);
        }
    }
    //endregion ::::::::::::::::::::::::::::::::::::::::::

    //region :::::::::::::::::::::::::::::::::::::::::: View Callback
    @Override
    public void setupAdapter(List<CharacterViewModel> list) {
        Log.d(TAG, "setupAdapter: " + mFragmentHouseID + " " + list.size());
        if (list == null || list.isEmpty()) {
            showMessage(R.string.error_db_empty);
        } else {
            if (mAdapter != null) mAdapter.setItems(list);
        }
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(mBinding.coordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showMessage(@StringRes int messageId) {
        showMessage(getString(messageId));
    }

    @Override
    public void showError(Throwable error) {
        showMessage(error.getMessage());
    }

    @Override
    public void showLoad() {
        if (mCallbacks != null) mCallbacks.showLoad();
    }

    @Override
    public void hideLoad() {
        if (mCallbacks != null) mCallbacks.hideLoad();
    }
    //endregion ::::::::::::::::::::::::::::::::::::::::::

    //region :::::::::::::::::::::::::::::::::::::::::: RecyclerView
    private void initRecycleView() {
        mBinding.charList.setLayoutManager(new LinearLayoutManager(getActivity()));

        mAdapter = new RecyclerBindingAdapter<>(
                R.layout.item_characters_list,
                BR.item,
                new ArrayList<>(),
                this::onViewProfileClick);

        mBinding.charList.smoothScrollToPosition(0);
        mBinding.charList.swapAdapter(mAdapter, false);
    }

    //region :::::::::::::::::::::::::::::::::::::::::: onClick
    private void onViewProfileClick(int position) {
        if (mAdapter == null || mAdapter.getItems().size() <= position) return;
        mPresenter.startCharacterScreenActivity(getContext(), mAdapter.getItems().get(position));
    }
}
