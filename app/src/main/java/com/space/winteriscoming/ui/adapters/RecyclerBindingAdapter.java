package com.space.winteriscoming.ui.adapters;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"unused", "unchecked"})
public class RecyclerBindingAdapter<T> extends RecyclerView.Adapter<RecyclerBindingAdapter.BindingHolder> {

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    private int holderLayout, variableId;
    private List<T> items = new ArrayList<>();
    private OnItemClickListener mItemClickListener;
    private int lastAnimatedPosition = -1;

    private boolean delayEnterAnimation = true;

    //region :::::::::::::::::::::::::::::::::::::::::: Adapter
    public RecyclerBindingAdapter(int holderLayout, int variableId, List<T> items) {
        this.holderLayout = holderLayout;
        this.variableId = variableId;
        this.items = items;
    }

    public RecyclerBindingAdapter(int holderLayout, int variableId, List<T> items, OnItemClickListener onItemClickListener) {
        this.mItemClickListener = onItemClickListener;
        this.holderLayout = holderLayout;
        this.variableId = variableId;
        this.items = items;
    }

    @Override
    public RecyclerBindingAdapter.BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(holderLayout, parent, false);
        return new BindingHolder(v, mItemClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerBindingAdapter.BindingHolder holder, int position) {
        final T item = items.get(position);
        holder.getBinding().setVariable(variableId, item);
    }

    //endregion :::::::::::::::::::::::::::::::::::::::::: Adapter

    //region :::::::::::::::::::::::::::::::::::::::::: Base Methods
    @Override
    public int getItemCount() {
        return items.size();
    }

    public List<T> getItems() {
        return items;
    }
    //endregion :::::::::::::::::::::::::::::::::::::::::: Base Methods

    //region :::::::::::::::::::::::::::::::::::::::::: Adapter Utils
    public void setItems(List<T> userEntities) {
        synchronized (this) {
            items.addAll(userEntities);
            notifyDataSetChanged();
        }
    }
    public void setOnItemClickListener(OnItemClickListener cLickListener) {
        this.mItemClickListener = cLickListener;
    }

    public void clearItems() {
        items.clear();
        notifyDataSetChanged();
    }

    //endregion :::::::::::::::::::::::::::::::::::::::::: Adapter Utils

    //region :::::::::::::::::::::::::::::::::::::::::: ViewHolder
    public static class BindingHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public BindingHolder(View view, OnItemClickListener clickListener) {
            super(view);
            binding = DataBindingUtil.bind(view);
            binding.getRoot().setOnClickListener(v -> {
                if (clickListener != null) {
                    clickListener.onItemClick(getAdapterPosition());
                }
            });
        }

        public ViewDataBinding getBinding() {
            return binding;
        }
    }
    //endregion :::::::::::::::::::::::::::::::::::::::::: ViewHolder
}
