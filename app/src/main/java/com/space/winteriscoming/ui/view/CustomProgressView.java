package com.space.winteriscoming.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Region;
import android.util.AttributeSet;
import android.widget.ImageView;

public class CustomProgressView extends ImageView {

    private int max = 100;
    private int curProgress = 0;

    public CustomProgressView(Context context) {
        super(context);
    }

    public CustomProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public int getMax() {
        return max;
    }

    public int getCurProgress() {
        return curProgress;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public void setCurProgress(int curProgress) {
        this.curProgress = curProgress;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.clipRect(0f, getHeight() - (getHeight() * curProgress / max), getWidth(), getHeight(), Region.Op.REPLACE);
        super.onDraw(canvas);
    }
}
