package com.space.winteriscoming.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.space.winteriscoming.common.CustomApp;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Helper class to work with UI
 */
@SuppressWarnings({"deprecation"})
public class AppUtils {
    //region :::::::::::::::::::::::::::::::::::::::::: Network
    public static Boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) CustomApp.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }
    //endregion ::::::::::::::::::::::::::::::::::::::::::

    //region :::::::::::::::::::::::::::::::::::::::::: Converters
    public static String getHouseId(String url) {
        return url == null ? "" : url.replace(AppConfig.BASE_URL + "houses/", "");
    }

    public static String getCharacterId(String url) {
        return url == null ? "" : url.replace(AppConfig.BASE_URL + "characters/", "");
    }
    //endregion ::::::::::::::::::::::::::::::::::::::::::

    //region :::::::::::::::::::::::::::::::::::::::::: Comparison
    public static <T> boolean compareLists(final Collection<T> list1, final Collection<T> list2) {
        ArrayList<T> tempList1 = new ArrayList<>(list1);
        for (T o : list2) {
            if (!tempList1.remove(o)) {
                return false;
            }
        }
        return tempList1.isEmpty();
    }

    public static boolean equals(Object a, Object b) {
        return (a == null) ? (b == null) : a.equals(b);
    }

    /**
     * checks all args if they equals null or empty
     *
     * @param args array of args
     * @return true if null or empty
     */
    public static boolean isEmptyOrNull(Object... args) {
        for (Object s : args) {
            if (s == null || s.toString().trim().isEmpty())
                return true;
        }
        return false;
    }
    //endregion ::::::::::::::::::::::::::::::::::::::::::
}