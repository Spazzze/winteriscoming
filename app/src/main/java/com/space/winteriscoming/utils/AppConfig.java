package com.space.winteriscoming.utils;

public interface AppConfig {
    String DB_NAME = "GameOfThrones-db";

    //Network timeouts
    long MAX_CONNECTION_TIMEOUT = 10000;
    long MAX_READ_TIMEOUT = 10000;
    long MAX_WRITE_TIMEOUT = 10000;

    //API
    String BASE_URL = "http://www.anapioficeandfire.com/api/";

    //Splash screen fade-in delay
    int SPLASH_FADE_DELAY = 3000;

    int VIEWPAGER_PAGE_COUNT = 3;

}
