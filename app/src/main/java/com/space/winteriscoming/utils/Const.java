package com.space.winteriscoming.utils;

import java.util.ArrayList;
import java.util.List;

public class Const {

    //Debug TAG
    public static final String TAG_PREFIX = "DEV ";

    public static final String HOUSE_FRAGMENT_KEY = "HOUSE_FRAGMENT_KEY";

    //Serialization
    public static final String PARCELABLE_KEY_CHARACTER_ID = "PARCELABLE_KEY_CHARACTER_ID";
    public static final String PARCELABLE_KEY_CHARACTER_LIST = "PARCELABLE_KEY_CHARACTER_LIST";
    public static final String PARCELABLE_KEY_CHARACTER = "PARCELABLE_KEY_CHARACTER";
    public static final String PARCELABLE_KEY_HOUSEID = "PARCELABLE_KEY_HOUSEID";
    public static final String PARCELABLE_KEY_SPLASH = "PARCELABLE_KEY_SPLASH";

    //Houses
    public static final String LANNISTER_ID = "229";
    public static final String STARK_ID = "362";
    public static final String TARGARYEN_ID = "378";
    public static final List<String> HOUSES_IDS = new ArrayList<String>() {{
        add(Const.LANNISTER_ID);
        add(Const.STARK_ID);
        add(Const.TARGARYEN_ID);
    }};
}
