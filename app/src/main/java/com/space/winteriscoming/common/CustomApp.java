package com.space.winteriscoming.common;

import android.app.Application;
import android.content.Context;

import com.facebook.stetho.Stetho;
import com.space.winteriscoming.data.storage.models.DaoMaster;
import com.space.winteriscoming.data.storage.models.DaoSession;
import com.space.winteriscoming.utils.AppConfig;

public class CustomApp extends Application {
    private static Context sContext;
    private static DaoSession sDaoSession;

    public static Context getContext() {
        return sContext;
    }

    public static DaoSession getDaoSession() {
        return sDaoSession;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = this;
        sDaoSession = new DaoMaster(new DaoMaster.DevOpenHelper(this, AppConfig.DB_NAME).getWritableDb()).newSession();

        Stetho.initializeWithDefaults(this);
    }
}
